## PAGOS STRIPE

PERIFERICOS

### Modalidad de pago al estilo datafono 

https://stripe.com/docs/terminal/quickstart
https://github.com/stripe-samples/sample-terminal-ios-app

### Tarjeta fisica o virtual de Stripe

https://stripe.com/issuing
https://github.com/stripe-samples/issuing


TIPO DE CARGO

### Directo

https://stripe.com/docs/connect/direct-charges
https://github.com/stripe-samples/connect-direct-charge-checkout

### Destino

https://stripe.com/docs/connect/destination-charges
https://github.com/stripe-samples/connect-destination-charge


TIEMPOS DE PAGO

### Pagos Agendados

https://stripe.com/docs/connect/manage-payout-schedule

### Manuales

https://stripe.com/docs/connect/manual-payouts

### Instantaneos

https://stripe.com/docs/connect/instant-payouts

### Cruzar Fronteras

https://stripe.com/docs/connect/cross-border-payouts

### Reversiones

https://stripe.com/docs/connect/payout-reversals

### Reembolsos

https://stripe.com/docs/refunds

https://gitlab.com/daicarjim/stripe-refunds-use-cases



MODELO DE NEGOCIO


### Vendedor al por menor

https://stripe.com/docs/payments/checkout
https://github.com/stripe-samples/checkout-one-time-payments

### Link

https://stripe.com/docs/payments/payment-links/overview#lifecycle-of-a-payment-link

### SAAS y Suscripciones

https://stripe.com/docs/billing
https://github.com/stripe-samples/subscription-use-cases

## PLATAFORMAS Y MARKETPLACES

uno a uno

https://stripe.com/docs/connect/enable-payment-acceptance-guide
https://github.com/stripe/stripe-connect-custom-rocketdeliveries


uno a muchos

https://stripe.com/docs/connect/collect-then-transfer-guide


reteniendo fondos

https://stripe.com/docs/connect/account-balances
https://github.com/stripe-samples/placing-a-hold


debitar cuentas


suscripciones


https://github.com/stripe/stripe-demo-connect-roastery-saas-platform


link

https://stripe.com/docs/connect/payment-links


### Creando Economia

https://stripe.com/use-cases/creator-economy



Basado en https://stripe.com/guides/introduction-to-online-payments#for-platforms-and-marketplaces
